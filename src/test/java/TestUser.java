import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import model.UserModel;
import org.hamcrest.Matchers;

import java.util.concurrent.TimeUnit;

import static  io.restassured.RestAssured.*;
import static io.restassured.matcher.RestAssuredMatchers.*;
import static org.hamcrest.Matchers.*;

/** Class represent testing of REST-API from https://reqres.in/*/
public class TestUser {

    private RequestSpecification requestSpecification;
    private ResponseSpecification responseSpecification;

    public TestUser() {

        requestSpecification = new RequestSpecBuilder()
                .setBaseUri("https://reqres.in")
                .setBasePath("/api")
                .setContentType(ContentType.JSON)
                .log(LogDetail.ALL)
                .build();

        responseSpecification = new ResponseSpecBuilder()
                .expectStatusCode(200)
                .expectResponseTime(Matchers.lessThanOrEqualTo(2000L),TimeUnit.MILLISECONDS)
                .build();
    }

    public void getUser() {
        UserModel testUser = new UserModel();

        given()
                .body(testUser)
                .when()
                .get()
                .then()
                .body(Matchers.equalTo(testUser));
    }
}
